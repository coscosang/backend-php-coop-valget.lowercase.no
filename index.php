<?
include(__DIR__ . '/class.app.php');
$app = new App;


switch ($_SERVER["REQUEST_METHOD"]) {
  case 'GET':
    $app->showPage();
    break;
  case 'POST':
    $app->saveImage();
    break;
  
  default:
    break;
}


die(json_encode($app->arResult));