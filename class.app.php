<?

/**
 * Save image to server and return url
 * hello@soft-angel.ru
 */
class App
{

  public $arResult = array();
  public $isShortUrl = true;

  function __construct(){
     header("Access-Control-Allow-Origin: *");
     ini_set('memory_limit', '256M');
     set_time_limit (10);
  }

  public function saveImage()
  {
    $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING);
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    //var_dump($data);
    $id = rand(1000, 9999);
    if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
        $data = substr($data, strpos($data, ',') + 1);
        $type = strtolower($type[1]); // jpg, png, gif

        if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
            $this->arResult['ERROR'][] = 'Invalid image type';
        }

        $data = base64_decode($data);
        if (!$data){
            $this->arResult['ERROR'][] = 'base64_decode failed';
            return false;
        }
        $md5 = md5($data);
        $idExistFile = $this->getIDExistFileMd5($md5);
        //var_dump($idExistFile);
        if(!$idExistFile){
          $imgPath = __DIR__ . "/images/{$md5}-{$id}.{$type}";
          if (!file_put_contents($imgPath, $data)){
             //$this->resizeImage($imgPath);
              $this->arResult['ERROR'][] = 'Failed save file to server';
          }else{
              file_put_contents(__DIR__ . "/titles/{$id}", $title);
          }
        }else{
          $id = $idExistFile;
          $this->arResult['MESSAGE'][] = 'Return save image exist';
        }

        $this->arResult['URL'] = $this->getDomain() . ($this->isShortUrl?'/':'/?id=') . $id;

    } else {
        $this->arResult['ERROR'][] = 'Did not match data URI with image data';
    }
    
  }

  private function resizeImage($imgPath)
  {
    include("./image-toolkit/AcImage.php");

    AcImage::setTransparency(false);
    AcImage::setQuality(75);
    AcImage::setRewrite(true);
    $img = AcImage::createImage($imgPath);
    $img->resizeByWidth(1000);
    $img->save($imgPath);
  }
  private function getIDExistFileMd5($md5) {
    $arFiles = glob(__DIR__ . "/images/{$md5}*");

    if(count($arFiles) > 0){
      preg_match("/-(.*)./is", basename($arFiles[0]), $arr);
      return preg_replace("/[^0-9]/", '', $arr[1]);
    }
    return false;
  }
  private function getDomain()
  {
      if (isset($_SERVER['HTTPS'])) {
          $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
      } else {
          $protocol = 'http';
      }
      return $protocol . "://" . $_SERVER['HTTP_HOST'];
  }
  public function showPage()
  {
    
      $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
      $id = (empty($id))?preg_replace("/[^0-9]/", '', parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)):$id;

      $title = file_get_contents(__DIR__ . "/titles/{$id}");
      $arFiles = glob(__DIR__ . "/images/*-{$id}*");

      $html = file_get_contents(__DIR__ . '/page.tmpl');
      if(isset($arFiles[0])){
        $fileUrl = str_replace(__DIR__ , null, $arFiles[0]);
        die(str_replace(array('#TITLE#', '#IMAGE#', '#DOMAIN#', '#URI#'), array($title, $fileUrl, $this->getDomain(), $_SERVER["REQUEST_URI"]), $html));
      }else{
        $this->arResult['ERROR'][] = "Image id: {$id} not found";
      }
  }
}

